* remove *.git* direcory and initialize your own repo
* change name in README.md
* change name in Makefile
* set proper "DEFS" value in Makefile
* change device field in .vscode/launch.json
* change defines field in .vscode/c_cpp_properties.json
* if you using FreeRTOS follow instructions in README and uncommenst FreeRTOS sections in Makefile