# stm32f4xx

## How to start:

- clone or download this repo

```bash
git clone https://gitlab.com/stm32ru/templates/stm32f4xx.git __projec_name__
```

- follow _TODO.md_!!
- add my stm32 utils if you want, example:

```bash
mkdir -p libs && cd libs
git submodule add https://gitlab.com/stm32ru/utils/ru_stm32libs.git
```

update submodules

```bash
git submodule update --init --recursive
```

arm toolchain env (can be added to _~/.profile_ file)

```bash
export ARM_TOOLCHAIN=~/embedded/gcc-arm-none-eabi-8/bin
```

## FreeRTOS

download FreeRTOSv10.2.1 and copy _./FreeRTOSv10.2.1/FreeRTOS/Source_ into project directory

```bash
cp -r ~/Downloads/FreeRTOSv10.2.1/FreeRTOS/Source ./libs/FreeRTOS
```

copy or prepare _FreeRTOSConfig.h_ file into ./src/

```bash
cp ~/Downloads/FreeRTOSv10.2.1/FreeRTOS/Demo/CORTEX_M4F_STM32F407ZG-SK/FreeRTOSConfig.h ./src/
```

uncomment FreeRTOS sections in Makefile
