# project name
PROJECT	= stm32fxx

TOOLCHAIN_PATH = $(ARM_TOOLCHAIN)/
CCPREFIX   = $(TOOLCHAIN_PATH)arm-none-eabi-
CC         = $(CCPREFIX)gcc
CP         = $(CCPREFIX)objcopy
AS         = $(CCPREFIX)gcc -x assembler-with-cpp
GDB        = $(CCPREFIX)gdb
HEX        = $(CP) -O ihex
BIN        = $(CP) -O binary -S
MCU        = cortex-m4

# defines
DEFS = -DHSE_VALUE=48000000 -DSTM32F411xE -DUSE_FULL_ASSERT # -DTRACE -DDEBUG -DOS_USE_TRACE_ITM

# optimalization
OPT = -Os

# linker scripts
LINKERSCRIPTS = -T./ldscripts/stm32_flash.ld

INCDIRS = \
	./src \
	./system/include \
	./system/include/cmsis \
	./libs/ru_stm32libs/f4 \
	# ./libs/FreeRTOS/include \
	# ./libs/FreeRTOS/portable/GCC/ARM_CM3 \

ASM_SRC = ./system/startup_stm32f4xx.s

SRC = \
	./src/main.c \

# system
SRC += \
	./system/src/cortexm/_initialize_hardware.c \
	./system/src/cortexm/_reset_hardware.c \
	./system/src/cortexm/exception_handlers.c \
	./system/src/newlib/assert.c \
	./system/src/newlib/_exit.c \
	./system/src/newlib/_sbrk.c \
	./system/src/newlib/_startup.c \
	./system/src/newlib/_syscalls.c \
	./system/src/diag/Trace.c \
	./system/src/diag/trace_impl.c \
	./system/src/cmsis/vectors_stm32f4xx.c \
	./system/src/cmsis/system_stm32f4xx.c \

# # ru_stm32libs
# SRC += \
# 	./libs/ru_stm32libs/f4/gpio.c \
# 	./libs/ru_stm32libs/f4/usart.c \
# 	./libs/ru_stm32libs/f4/rcc.c \

# # FreeRTOS
# SRC += \
# 	./libs/FreeRTOS/croutine.c \
# 	./libs/FreeRTOS/event_groups.c \
# 	./libs/FreeRTOS/list.c \
# 	./libs/FreeRTOS/queue.c \
# 	./libs/FreeRTOS/stream_buffer.c \
# 	./libs/FreeRTOS/tasks.c \
# 	./libs/FreeRTOS/timers.c \

# # FreeRTOS specific to the project
# SRC += \
# 	./libs/FreeRTOS/portable/GCC/ARM_CM3/port.c \
# 	./libs/FreeRTOS/portable/MemMang/heap_4.c \

INCDIR  = $(patsubst %,-I%, $(INCDIRS))
OBJS  	= $(ASM_SRC:.s=.o) $(SRC:.c=.o)
DEPS := $(OBJS:.o=.d)
MCFLAGS = -mcpu=$(MCU)
ASFLAGS = $(MCFLAGS) -g -gdwarf-2 -mthumb -Wa,-amhls=$(<:.s=.lst) 

CFLAGS = -g $(OPT) -Wall $(LINKERSCRIPTS)
CFLAGS += -mlittle-endian -mthumb $(MCFLAGS) -mthumb-interwork
CFLAGS += -mfloat-abi=hard -mfpu=fpv4-sp-d16
CFLAGS += $(DEFS)

all: $(OBJS) $(PROJECT).elf $(PROJECT).hex $(PROJECT).bin
	$(CCPREFIX)size -A $(PROJECT).elf
	$(CCPREFIX)size -B $(PROJECT).elf
	$(CCPREFIX)size -B $(PROJECT).hex
 
%.o: %.c
	$(CC) -c $(CFLAGS) -I . $(INCDIR) $< -o $@

%.o: %.s
	$(AS) -c $(ASFLAGS) $< -o $@

%.elf: $(OBJS)
	$(CC) $(OBJS) $(LDFLAGS) $(LIBS) -o $@

%.hex: %.elf
	$(HEX) $< $@
	
%.bin: %.elf
	$(BIN)  $< $@
		
clean:
	-rm -rf $(OBJS)
	-rm -rf $(DEPS)
	-rm -rf $(PROJECT).elf
	-rm -rf $(PROJECT).map
	-rm -rf $(PROJECT).hex
	-rm -rf $(PROJECT).bin
	-rm -rf $(SRC:.c=.lst)
	-rm -rf $(ASRC:.s=.lst)

-include $(DEPS)
